import tensorflow as tf
import pandas as pd
import tensorflow_hub as hub
import os
import re
from keras import backend as K
import keras.layers as layers
from keras.models import Model, load_model
from keras.engine import Layer
import numpy as np
from sklearn.metrics import f1_score
from sklearn.metrics import auc
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_recall_curve
# from keras.callbacks import Callback
from sklearn.model_selection import KFold

from skmultilearn.problem_transform import LabelPowerset # pip3 install scikit-multilearn
from imblearn.over_sampling import RandomOverSampler # pip3 install imblearn


import random
sess = tf.Session()
K.set_session(sess)


# class Metrics(Callback):
# 	def on_train_begin(self, logs={}):
# 		self.val_f1s = []
# 		self.val_recalls = []
# 		self.val_precisions = []
# 	def on_epoch_end(self, epoch, logs={}):
# 		val_predict = (np.asarray(self.model.predict(self.model.validation_data[0]))).round()
# 		val_targ = self.model.validation_data[1]
# 		_val_f1 = f1_score(val_targ, val_predict)
# 		_val_recall = recall_score(val_targ, val_predict)
# 		_val_precision = precision_score(val_targ, val_predict)
# 		self.val_f1s.append(_val_f1)
# 		self.val_recalls.append(_val_recall)
# 		self.val_precisions.append(_val_precision)
# 		print (" — val_f1: %f — val_precision: %f — val_recall %f" .format(_val_f1, _val_precision, _val_recall))
# 		return 

# metrics = Metrics()


# def custom_f1(y_true, y_pred):    
# 	def recall_m(y_true, y_pred):
# 		TP = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
# 		Positives = K.sum(K.round(K.clip(y_true, 0, 1)))
# 		recall = TP / (Positives+K.epsilon())    
# 		return recall 
# 	def precision_m(y_true, y_pred):
# 		TP = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
# 		Pred_Positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
# 		precision = TP / (Pred_Positives+K.epsilon())
# 		return precision 
# 	precision, recall = precision_m(y_true, y_pred), recall_m(y_true, y_pred)
# 	return 2*((precision*recall)/(precision+recall+K.epsilon()))

	
class ElmoEmbeddingLayer(Layer):
	def __init__(self, **kwargs):
		self.dimensions = 1024
		self.trainable=True
		super(ElmoEmbeddingLayer, self).__init__(**kwargs)	
	def build(self, input_shape):
		self.elmo = hub.Module('https://tfhub.dev/google/elmo/3', trainable=self.trainable,name="{}_module".format(self.name))
		self._trainable_weights += tf.compat.v1.trainable_variables(scope="^{}_module/.*".format(self.name))
		super(ElmoEmbeddingLayer, self).build(input_shape)	
	def call(self, x, mask=None):
		result = self.elmo(K.squeeze(K.cast(x, tf.string), axis=1),
					  as_dict=True,
					  signature='default',
					  )['default']
		return result	
	def compute_mask(self, inputs, mask=None):
		return K.not_equal(inputs, '--PAD--')	
	def compute_output_shape(self, input_shape):
		return (input_shape[0], self.dimensions)



def build_model(): 
	input_text = layers.Input(shape=(1,), dtype="string")
	embedding_lyr = ElmoEmbeddingLayer()(input_text)
	dense = layers.Dense(512, activation='relu')(embedding_lyr)
	pred = layers.Dense(12, activation='softmax')(dense)
	model = Model(inputs=[input_text], outputs=pred)
	model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
	model.summary()
	return model

if __name__ == "__main__":


	fraction = 1
	E=10
	batch_size = 512
	threads = 10
	multi=True


	Dataset_ori= pd.read_csv('/storage/work/n/nmc5751/ELMo/OPP15/Cleaned_multilabel_dataset.txt',sep='\t')
	del Dataset_ori['Other']
	Dataset_ori["Id"] = Dataset_ori['website'] + Dataset_ori['segment ID'].astype('str')

	# Dataset_ori=Dataset_ori.sample(frac=fraction)

	labels_order = Dataset_ori.columns[3:-1]

	all_websites = Dataset_ori['website'].unique()

	lp = LabelPowerset()
	ros = RandomOverSampler(random_state=42)

	for count in range(10):
		random.shuffle(all_websites)

		train_webs = all_websites[0:75]

		train_df = Dataset_ori[Dataset_ori['website'].isin(train_webs)]
		test_df = Dataset_ori[~Dataset_ori['website'].isin(train_webs)]


		train_df=train_df.sample(frac=fraction)
		test_df=test_df.sample(frac=fraction)

		train_text_ori = train_df['alltext'].tolist()
		train_text_ori = [' '.join(t.split()) for t in train_text_ori]
		train_text_ori = np.array(train_text_ori, dtype=object)[:, np.newaxis]
		train_label_ori = train_df.values[:,3:-1]

		test_text = test_df['alltext'].tolist()
		test_text = [' '.join(t.split()) for t in test_text]
		test_text = np.array(test_text, dtype=object)[:, np.newaxis]
		test_label = test_df.values[:,3:-1]

		print('Before upsampling: ',train_text_ori.shape,train_label_ori.shape,test_text.shape,test_label.shape)

		yt = lp.transform(train_label_ori.astype('int'))
		train_text, y_resampled = ros.fit_sample(train_text_ori.astype('str'), yt)

		train_label = lp.inverse_transform(y_resampled).toarray()

		# train_text=train_text_ori
		# train_label = train_label_ori

		print('After Up-sampling',train_text.shape,train_label.shape,test_text.shape,test_label.shape)


		model = build_model()

	# cv = KFold(n_splits=10, random_state=420, shuffle=False)

	# count = 0
	# for train_index, test_index in cv.split(label):

	# 	print('Running the Fold',count)

	# 	train_text, test_text, train_label, test_label = text[train_index], text[test_index], label[train_index], label[test_index]
		# msk = np.random.rand(len(Dataset_ori)) < 0.75

		# train = Dataset_ori[msk]
		# test = Dataset_ori[~msk]

		# train_df, test_df = train,test
		# print(train_df.tail(3))

		print('train X and Y',train_text.shape,train_label.shape)
		print('test X and Y',test_text.shape,test_label.shape)


		history = model.fit(train_text, train_label,validation_data=(test_text, test_label),epochs=E,batch_size=batch_size,use_multiprocessing=multi,workers=threads)

		training_res = pd.DataFrame(history.history['val_loss'],columns=['val_loss'])
		training_res['val_accuracy'] = history.history['val_accuracy']
		training_res['loss'] = history.history['loss']
		training_res['accuracy'] = history.history['accuracy']
		training_res['Epoch'] = training_res.index
		training_res['Fold'] = count

		if count==0:
			training_res.to_csv('/storage/home/nmc5751/work/ELMo/results/training_metrics_per_epoch_BS_1024.csv',header=True,mode='a',sep='\t')
		else:
			training_res.to_csv('/storage/home/nmc5751/work/ELMo/results/training_metrics_per_epoch_BS_1024.csv',header=False,mode='a',sep='\t')

		model.save('/storage/work/n/nmc5751/ELMo/trained_model/ElmoModel_final_fold_{}_BS_1024.h5'.format(count))
		pre_save_preds = model.predict(test_text) 

		pred_binary = np.where(pre_save_preds>=0.5,1,0)

		np.savez('/storage/home/nmc5751/work/ELMo/results/test_prediction_results_final_fold_{}_BS_1024'.format(count),p=pred_binary,p_prob=pre_save_preds, a=test_label,labels=labels_order)

		result_scores = pd.DataFrame()

		for category in range(train_label.shape[1]):
			label_name = labels_order[category]

			lr_precision, lr_recall, _ = precision_recall_curve(test_label[:,category].astype('float'), pre_save_preds[:,category])
			lr_f1, lr_auc = f1_score(test_label[:,category].astype(int), pred_binary[:,category]), auc(lr_recall, lr_precision)

			if sum(test_label[:,category])==0 and sum(pred_binary[:,category])==0:
				tp=0
				fp=0
				tn=len(pred_binary[:,category])
				fn=0
			else:
				tn, fp, fn, tp = confusion_matrix(test_label[:,category].astype(int), pred_binary[:,category]).ravel()
		
			if tp+fp==0:
				precision = 0
			else:
				precision = float(tp)/float(tp+fp)		

			if tp+fn==0:
				recall = 0
			else:
				recall = float(tp)/float(tp+fn)
			

			df = pd.DataFrame([{'category':label_name,'tp':tp,'fp':fp,'tn':tn,'fn':fn,'recall':recall,'precision':precision,'f1':lr_f1,'auc':lr_auc}])

			result_scores = result_scores.append(df)

		if count==0:
			result_scores.to_csv('/storage/home/nmc5751/work/ELMo/results/result_scores_BS_1024.csv',header=True,mode='a',sep='\t')
		else:
			result_scores.to_csv('/storage/home/nmc5751/work/ELMo/results/result_scores_BS_1024.csv',header=False,mode='a',sep='\t')

		count=count+1
	# result_scores.to_csv('/storage/home/nmc5751/work/ELMo/results/result_scores_final.csv')

# Clear and load model
# model = None
# model = build_model()
# model.load_weights('ElmoModel.h5')

# post_save_preds = model.predict(test_text[0:100]) 
# all(pre_save_preds == post_save_preds) 

